# @timeyurah/weekdays-string-builder

[![npm (scoped)](https://img.shields.io/badge/npm-v1.0.2-blue.svg)](https://gitlab.com/yurah/shared/weekdays-string-builder)

## Objetivo

Construir string separadas por vírgula de modo legível para homanos, onde o separador entre os dois últimos dias seja o caracter 'e'.


## Instalação

```
npm install @timeyurah/weekdays-string-builder
```

## Uso

```
const weekdays = require("@timeyurah/weekdays-string-builder");
const yourList = ["segunda", "domingo", "quinta"];

const string = weekdays(yourList);
// => Expected output:
//    "Segunda, quinta e domingo"
```